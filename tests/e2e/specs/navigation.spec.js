describe('Check if authenticated', () => {
  beforeEach(() => {
    cy.visit('http://localhost:8080');
    cy.get("[type='email']").clear();
    cy.get("[type='password']").clear();
    cy.get("[type='email']").type('roshan@gritfeat.com');
    cy.get("[type='password']").type('password');
    cy.get('.q-btn').click();
  })
  it('check all side navs', () => {
    cy.viewport(1920,1080);
    cy.get("[type='email']").clear();
    cy.get("[type='password']").clear();
    cy.get("[type='email']").type('roshan@gritfeat.com');
    cy.get("[type='password']").type('password');
    cy.get('.q-btn').click();
    cy.location('pathname').should('not.contain','login');
    cy.get('.q-item .q-item__label').contains('Dashboard').click();
    cy.location('pathname').should('contain','/');
    cy.get('.q-item .q-item__label').contains('Day Book').click();
    cy.location('pathname').should('contain','day-book');
    cy.get('.q-item .q-item__label').contains('Purchase History').click();
    cy.location('pathname').should('contain','purchase-history');
    cy.get('.q-item .q-item__label').contains('Sales History').click();
    cy.location('pathname').should('contain','sales-history');
    cy.get('.q-item .q-item__label').contains('Employee History').click();
    cy.location('pathname').should('contain','employee-history');
    cy.get('.q-item .q-item__label').contains('Employee Records').click();
    cy.location('pathname').should('contain','employee-records');
    cy.get('.q-item .q-item__label').contains('Customers').click();
    cy.location('pathname').should('contain','customers');
    cy.get('.q-item .q-item__label').contains('Logout').click();
    cy.location('pathname').should('include', 'login')

  });
  it('check internal routes', () => {
    cy.viewport(1920,1080);
    cy.get('.q-item .q-item__label').contains('Day Book').click();
    cy.location('pathname').should('contain','day-book');
    cy.get('button .block').contains('Add New Transaction').click();
    cy.location('pathname').should('contain','add-transaction');
    cy.get('.q-item .q-item__label').contains('Purchase History').click();
    cy.location('pathname').should('contain','purchase-history');
    cy.get('button .block').contains('Add New Transaction').click();
    cy.location('pathname').should('contain','add-transaction');
    cy.get('.q-item .q-item__label').contains('Purchase History').click();
    cy.get('button .block').contains('Add New Payment').click();
    cy.location('pathname').should('contain','add-new-payment');
    // cy.get('button .block').should('contain','')
  });

});
