// https://docs.cypress.io/api/introduction/api.html

describe('Login Page Test', () => {
  it('Visits the app root url', () => {
    cy.viewport(1920,1080);
    cy.visit('http://localhost:8080/');
    cy.get("[type='email']").clear();
    cy.get("[type='password']").clear();
    cy.contains('.text-h2', 'Login');
    cy.get('.q-btn').click();
    cy.contains('.q-field__messages','Password is required');
    cy.get("[type='email']").type('roshan@gritfeat.com');
    cy.get("[type='password']").type('password');
    cy.get('.q-btn').click();
    cy.location('pathname').should('not.include', 'login')
    // cy.get("button[aria-label='Menu']").click();
    cy.get('.q-item .q-item__label').contains('Logout').click();
    cy.location('pathname').should('include', 'login')
  });
});
