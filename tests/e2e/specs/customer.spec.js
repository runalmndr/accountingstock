describe('Customer test',()=>{
 beforeEach(()=>{
   cy.viewport(1920,1080);
   cy.visit('http://localhost:8080');
   cy.get("[type='email']").clear();
   cy.get("[type='password']").clear();
   cy.get("[type='email']").type('roshan@gritfeat.com');
   cy.get("[type='password']").type('password');
   cy.get('.q-btn').click();
   cy.get('.q-item .q-item__label').contains('Customers').click();
   cy.location('pathname').should('contain','customers');
   cy.get('.text-h5').should('contain','Customers Record');
   cy.get('.q-btn span').contains('Add New Customer').click();
   cy.get('.text-h5.q-pb-md').should('contain','Add Customer');
 });
  it('Route Check',()=>{
    cy.get('.q-btn span').contains('Cancel').click();
    cy.get('.q-item .q-item__label').contains('Logout').click();
    cy.location('pathname').should('include', 'login')
 })

  it('Check if customer being add',()=>{



    cy.get("input").clear();
    cy.get('.q-field__messages.col').should('contain','Name is required');
    cy.get('.q-field__messages.col').should('contain','Company name is required');
    cy.get('.q-field__messages.col').should('contain','Address is required');
    cy.get('.q-field__messages.col').should('contain','Phone number is required');
    cy.get('.q-field__messages.col').should('contain','Balance is required');

    cy.get("[name='name']").type('John Doe');
    cy.get("[name='companyName']").type('Test Company');
    cy.get("[name='address']").type('Test Address');
    cy.get("[name='phone']").type('12346789');
    cy.get("[name='balance']").type('1234');

    cy.get('button span').contains('Save').click();

    cy.get("td").should('contain','John Doe');
    cy.get("td").should('contain','Test Company');
    cy.get("td").should('contain','Test Address');
    cy.get("td").should('contain','12346789');
    cy.get("td").should('contain','1234');
  });
});
