describe('My First Test', () => {
  it('Visits the app root url', () => {
    cy.visit('localhost:8081');
    cy.contains('h1', 'Welcome to Your Vue.js + TypeScript App');
  });
});
