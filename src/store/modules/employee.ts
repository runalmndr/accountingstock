import { GetterTree, MutationTree, ActionTree } from 'vuex';
import { StateInterface } from 'src/store';
import { APIDeleteEmployee, APIEditEmployee, APIGetAllEmployees, APIStoreAllEmployee, APIGetEmployees } from 'src/api/employee';
import { successNotify,errorNotify } from 'src/utils/notify';

export interface Employee {
  name: string;
  balance: number;
}

//Store Start
const getters: GetterTree<Employee, StateInterface> = {};
const state: any = {
  listOfEmployees: [],
  account: {}
};
const mutations = {
  setListOfEmployees(state, payload) {
    state.listOfEmployees = payload;
  },
  setEmployee(state, payload) {
    state.account = payload;
  }
};
const actions = {
  async loadEmployees({ commit }) {
    try{
    const { data, message } = await APIGetAllEmployees();
    // successNotify(message);
    commit('setListOfEmployees', data);
    }
    catch(e){
      errorNotify(e);
    }
  },
  async loadEmployeesNames({ commit }) {
    try{
    const { data, message } = await APIGetEmployees();
    // successNotify(message);
    commit('setListOfEmployees', data);
    }
    catch(e){
      errorNotify(e);
    }
  },

  async addEmployee({ commit }, data) {
    try{
    const { message } = await APIStoreAllEmployee(data);
    successNotify(message);
    }
    catch(e){
      errorNotify(e);
    }
  },
  async deleteEmployee({ commit }, id) {
    try{
    const { message } = await APIDeleteEmployee(id);
    successNotify(message);
    }
    catch(e){
      errorNotify(e);
    }
  },
  async editEmployee({ commit }, data) {
    try{
    const { message } = await APIEditEmployee(data._id, data);
    successNotify(message);
    }
    catch(e){
      errorNotify(e);
    }
  }
};

//exprt all data
export default {
  getters,
  state,
  mutations,
  actions
};
