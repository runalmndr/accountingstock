import { ActionTree, GetterTree, MutationTree } from 'vuex';
import { StateInterface } from 'src/store';
import { TRYITEMS } from 'src/utils/tryData';

export interface  Try{
  items : any;
}

const practise : Try = {
  items : TRYITEMS
}

//Store Start
const getters : GetterTree<Try, StateInterface>={}

const state ={
  ...practise
}
const mutations : MutationTree<Try> = {}
const actions : ActionTree<Try, StateInterface> = {}

export default {
  getters, state, mutations,actions
}
