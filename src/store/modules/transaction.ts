import { GetterTree, MutationTree, ActionTree } from 'vuex';
import { StateInterface } from 'src/store';
import {
  APIDeleteTransaction,
  APIEditTransaction,
  APIGetAllSalary,
  APIGetAllTransactions,
  APIStoreAllTransaction, APIStoreSalary,APIStoreCredit,
  APIEditCredit,
  APIGetAllCredit,APIEditSalary
} from 'src/api/transaction';
import { errorNotify, successNotify } from 'src/utils/notify';

export interface BooksOfTransaction {
  name: string;
  balance: number;
}

//Store Start
const getters: GetterTree<BooksOfTransaction, StateInterface> = {};
const state: any = {
  listOfTransactions: [],
  account: {}
};
const mutations = {
  setListOfTransactions(state, payload) {
    state.listOfTransactions = payload;
  },
  setTransaction(state, payload) {
    state.account = payload;
  }
};
const actions = {
  async loadTransactions({ commit }) {
    try{
    const { data, message } = await APIGetAllTransactions();
    commit('setListOfTransactions', data);
    }
    catch(e){
      errorNotify(e);
    }
  },
  async loadSalaries({ commit }) {
    try{
    const { data, message } = await APIGetAllSalary();
    commit('setListOfTransactions', data);
    }
    catch(e){
      errorNotify(e);
    }
  },
  async loadCredits({ commit }) {
    try{
    const { data, message } = await APIGetAllCredit();
    commit('setListOfTransactions', data);
    }
    catch(e){
      errorNotify(e);
    }
  },
  async addSalary({ commit }, data) {
    try{
    const { message } = await APIStoreSalary(data);
    successNotify(message);
    }
    catch(e){
      errorNotify(e);
    }
  },
  async addCredit({ commit }, data)   {
    try{
    const { message } = await APIStoreCredit(data);
    successNotify(message);
    }
    catch(e){
      errorNotify(e);
    }
  },
  async addTransaction({ commit }, data) {
    try{
    const { message } = await APIStoreAllTransaction(data);
    successNotify(message);
    }
    catch(e){
      errorNotify(e);
    }
  },
  async deleteTransaction({ commit }, id) {
    try {
      const { message } = await APIDeleteTransaction(id);
      successNotify(message);
    } catch (e) {
      errorNotify('Unable to delete transaction', e);
    }
  },
  async editTransaction({ commit }, data) {
    try{
    const { message } = await APIEditTransaction(data._id, data);
    successNotify(message);
    }
    catch(e){
      errorNotify(e);
    }
  },
  async editSalary({ commit }, data) {
    try{
    const { message } = await APIEditSalary(data._id, data);
    successNotify(message);
    }
    catch(e){
      errorNotify(e);
    }
  },
  async editCredit({ commit }, data) {
    try{
    const { message } = await APIEditCredit(data._id, data);
    successNotify(message);
    }
    catch(e){
      errorNotify(e);
    }
  }
};

//exprt all data
export default {
  getters,
  state,
  mutations,
  actions
};
