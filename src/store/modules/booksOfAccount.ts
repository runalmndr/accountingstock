import { GetterTree, MutationTree, ActionTree } from 'vuex';
import { StateInterface } from 'src/store';
import { APIDeleteAccount, APIEditAccount, APIGetAllAccounts, APIStoreAllAccount,APIGetOpeningClosing,APIGetEmployeeRecipient } from 'src/api/booksOfAccount';
import { errorNotify, successNotify } from 'src/utils/notify';

export interface BooksOfAccount {
  name: string;
  balance: number;
}

//Store Start
const getters: GetterTree<BooksOfAccount, StateInterface> = {};
const state: any = {
  listOfAccounts: [],
  account: {},
  openingClosing:{},
  employeeRecipient:[],
};
const mutations = {
  setListOfAccounts(state, payload) {
    state.listOfAccounts = payload;
  },
  setAccount(state, payload) {
    state.account = payload;
  },
  setOpeningClosing(state, payload) {
    state.openingClosing = payload;
  },
  setEmployeeRecipient(state, payload) {
    state.employeeRecipient = payload;
  },
};
const actions = {
  async loadAccounts({ commit }) {
    try{
      const { data, message } = await APIGetAllAccounts();
    commit('setListOfAccounts', data);
    }
    catch(e){
      errorNotify(e);
    }
  },
  async loadEmployeeRecipient({ commit }) {
    try{
      const { data, message } = await APIGetEmployeeRecipient();
    commit('setEmployeeRecipient', data);
    }
    catch(e){
      errorNotify(e);
    }
  },
  async loadOpeningClosing({ commit }) {
    try{
      const { data, message } = await APIGetOpeningClosing();
      commit('setOpeningClosing', data);
    }
    catch(e){
      errorNotify(e);
    }
  },
  async addAccount({ commit }, data) {
    try{
    const { message } = await APIStoreAllAccount(data);
    successNotify(message);
    }
    catch(e){
      errorNotify(e);
    }
  },
  async deleteAccount({ commit }, id) {
    try{
    const res = await APIDeleteAccount(id);
    successNotify('Data deleted Successfully');
    }
    catch(e){
      errorNotify(e);
    }
  },
  async editAccount({ commit }, data) {
    try{
    const res = await APIEditAccount(data._id, data);
    successNotify('Data Edited Successfully');
    }
    catch(e){
      errorNotify(e);
    }
  }
};

//exprt all data
export default {
  getters,
  state,
  mutations,
  actions
};
