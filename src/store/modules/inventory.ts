import { GetterTree, MutationTree, ActionTree } from 'vuex';
import { StateInterface } from 'src/store';
import {
  APIDeleteInventory,
  APIEditInventory,
  APIGetAllInventory,
  APIStoreInventory,
  APIEditInventoryComp,
  APIDeleteInventoryComp,
  APIAddExtraInventoryComp
} from 'src/api/inventory';
import { errorNotify, successNotify } from 'src/utils/notify';

export interface Inventory {
  name: string,
  minQuantity: number,
  quantity: number,
  category: string,
  price: number,
  type: string
}

//Store Start
const getters: GetterTree<Inventory, StateInterface> = {};
const state: any = {
  listOfInventory: [],
  account: {}
};
const mutations = {
  setListOfInventory(state, payload) {
    state.listOfInventory = payload;
  },
  setInventory(state, payload) {
    state.account = payload;
  }
};
const actions = {
  async loadInventory({ commit }) {
    try {
      const { data, message } = await APIGetAllInventory();
      // successNotify(message);
      commit('setListOfInventory', data);
    } catch (e) {
      errorNotify(e);
    }
  },
  async addInventory({ commit }, data) {
    try {
      const { message } = await APIStoreInventory(data);
      successNotify(message);
    } catch (e) {
      errorNotify('Unable to perform transaction', e);
    }
  },
  async deleteInventory({ commit }, id) {
    try {
      const { message } = await APIDeleteInventory(id);
      successNotify(message);
    } catch (e) {
      errorNotify(e);
    }
  },
  async deleteInventoryComp({ commit }, data) {
    try {
      const { message } = await APIDeleteInventoryComp(data.iId, data.cId);
      successNotify(message);
    } catch (e) {
      errorNotify(e);
    }
  },
  async editInventory({ commit }, data) {
    try {
      const { message } = await APIEditInventory(data._id, data);
      successNotify(message);
    } catch (e) {
      errorNotify(e);
    }
  },
  async editInventoryComp({ commit }, data) {
    try {
      const { message } = await APIEditInventoryComp(data._id, data);
      successNotify(message);
    } catch (e) {
      errorNotify(e);
    }
  },
  async addExtraInventoryComp({ commit }, data) {
    try {
      const { message } = await APIAddExtraInventoryComp(data._id, data);
      successNotify(message);
    } catch (e) {
      errorNotify(e);
    }
  }
};

//exprt all data
export default {
  getters,
  state,
  mutations,
  actions
};
