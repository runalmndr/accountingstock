import { ActionTree, GetterTree, MutationTree } from 'vuex';
import { StateInterface } from 'src/store';
import { generateStoreValues } from 'src/utils/base/storeGenerator';

const generatedStore = generateStoreValues('Example');
export interface IExample {
  exampleData: string;
}

const iExample: IExample = {
  exampleData: ''
};

//Store Start
const getters: GetterTree<IExample, StateInterface> = {};
const state = {
  iExample
};
const mutations: MutationTree<IExample> = {};
const actions: ActionTree<IExample, StateInterface> = {};

export default {
  getters, state, mutations, actions
};
