import { GetterTree, MutationTree, ActionTree } from 'vuex';
import { StateInterface } from 'src/store';
import {
  APIGetAllBill,
  APIStoreBill,
  APIDeleteBill,
  APIEditBill,
  APIUpdateAmtPaid,
  APIGetBillById,
  APIStoreBillOrderItems,
  APIDeleteBillOrderItem,
  APIEditBillOrderItem,

} from 'src/api/bill';
import { errorNotify, successNotify } from 'src/utils/notify';

export interface BooksOfTransaction {
  name: string;
  balance: number;
}

//Store Start
const getters: GetterTree<BooksOfTransaction, StateInterface> = {};
const state: any = {
  listOfBills: [],
  bill: {}
};
const mutations = {
  setListOfTransactions(state, payload) {
    state.listOfBills = payload;
  },
  setTransaction(state, payload) {
    state.bill = payload;
  }
};
const actions = {
  async loadBill({ commit }) {
    try{
    const { data, message } = await APIGetAllBill();
    // successNotify(message);
    commit('setListOfTransactions', data);
    }
    catch(e){
      errorNotify(e);
    }
  },
  async loadBillById({ commit },id) {
    try{
    const { data, message } = await APIGetBillById(id);
    // successNotify(message);
    commit('setTransaction', data);
    }
    catch(e){
      errorNotify(e);
    }
  },

  async addBill({ commit }, data) {
    try{
    const { message } = await APIStoreBill(data);
    successNotify(message);
    }
    catch(e){
      errorNotify(e);
    }
  },
  async addBillOrderItem({ commit }, data) {
    try{
    const { message } = await APIStoreBillOrderItems(data.id,data);
    successNotify(message);
    }
    catch(e){
      errorNotify(e);
    }
  },
  async deleteBill({ commit }, id) {
    try {
      const { message } = await APIDeleteBill(id);
      successNotify(message);
    } catch (e) {
      errorNotify('Unable to delete transaction', e);
    }
  },
  async deleteBillOrderItem({ commit }, { billId, orderId }) {
    try {
    const {message} = await APIDeleteBillOrderItem(billId,orderId);
      successNotify(message);
    } catch (e) {
      errorNotify('Unable to delete transaction', e);
    }
  },
  async editBill({ commit }, data) {
    try{
    const { message } = await APIEditBill(data._id, data);
    successNotify(message);
    }
    catch(e){
      errorNotify(e);
    }
  },
  async editBillOrderItem({ commit }, data) {
    try{
    const { message } = await APIEditBillOrderItem(data._id, data);
    successNotify(message);
    }
    catch(e){
      errorNotify(e);
    }
  },
  async updateAmtPaid({ commit }, data,) {
    try{
    const { message } = await APIUpdateAmtPaid(data._id, data);
    successNotify(message);
    }
    catch(e){
      errorNotify(e);
    }
  }
};

export default {
  getters,
  state,
  mutations,
  actions
};
