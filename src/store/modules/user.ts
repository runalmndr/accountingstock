import { GetterTree, MutationTree, ActionTree } from 'vuex';
import { StateInterface } from 'src/store';
import { APIGetAllUsers, APIStoreUser,APIEditRecipient,APIGetAllRecipientName,APIDeleteRecipient } from 'src/api/user';
import { successNotify,errorNotify } from 'src/utils/notify';
import { APIDeleteTransaction, APIEditTransaction } from 'src/api/transaction';

export interface User {
  name: string;
  balance: number;
}

//Store Start
const getters: GetterTree<User, StateInterface> = {};
const state: any = {
  listOfUser: [],
  user: {}
};
const mutations = {
  setListOfUser(state, payload) {
    state.listOfUser = payload;
  },
  setUser(state, payload) {
    state.user = payload;
  }
};
const actions = {
  async loadUser({ commit }) {
    try{
    const { data, message } = await APIGetAllUsers();
    // successNotify(message);
    commit('setListOfUser', data);
    }
    catch(e){
      errorNotify(e);
    }
  },
  async loadRecipientName({ commit }) {
    try{
    const { data, message } = await APIGetAllRecipientName();
    // successNotify(message);
    commit('setListOfUser', data);
    }
    catch(e){
      errorNotify(e);
    }
  },
  async addUser({ commit }, data) {
    try{
    const { message } = await APIStoreUser(data);
    successNotify(message);
    }
    catch(e){
      errorNotify(e);
    }
  },
  async editRecipient({ commit }, data) {
    try{
      const { message } = await APIEditRecipient(data.rowId,data.userId, data);
      successNotify(message);
    }
    catch(e){
      errorNotify(e);
    }
  },
  async deleteRecipient({ commit }, id) {
    try {
      const { message } = await APIDeleteRecipient(id);
      successNotify(message);
    } catch (e) {
      errorNotify('Unable to delete recipient', e);
    }
  },
};

//exprt all data
export default {
  getters,
  state,
  mutations,
  actions
};
