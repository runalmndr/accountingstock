import { GetterTree, MutationTree, ActionTree } from 'vuex';
import { StateInterface } from 'src/store';
import {
  APIDeleteWarehouse,
  APIEditWarehouse,
  APIGetAllWarehouse, APIStoreWarehouse
} from 'src/api/warehouse';
import { successNotify,errorNotify } from 'src/utils/notify';

export interface BooksOfWarehouse {
  name: string;
  balance: number;
}

//Store Start
const getters: GetterTree<BooksOfWarehouse, StateInterface> = {};
const state: any = {
  listOfWarehouses: [],
  account: {}
};
const mutations = {
  setListOfWarehouses(state, payload) {
    state.listOfWarehouses = payload;
  },
  setWarehouse(state, payload) {
    state.account = payload;
  }
};
const actions = {
  async loadWarehouses({ commit }) {
    try{
    const { data, message } = await APIGetAllWarehouse();
    // successNotify(message);
    commit('setListOfWarehouses', data);
    }
    catch(e){
      errorNotify(e);
    }
  },
  async addWarehouse({ commit }, data) {
    try{
    const { message } = await APIStoreWarehouse(data);
    successNotify(message);
    }
    catch(e){
      errorNotify(e);
    }
  },
  async deleteWarehouse({ commit }, id) {
    try{
    const res = await APIDeleteWarehouse(id);
    successNotify('Data deleted Successfully');
    }
    catch(e){
      errorNotify(e);
    }
  },
  async editWarehouse({ commit }, data) {
    try{
    const res = await APIEditWarehouse(data._id, data);
    successNotify('Data deleted Successfully');
    }
    catch(e){
      errorNotify(e);
    }
  }
};

//exprt all data
export default {
  getters,
  state,
  mutations,
  actions
};
