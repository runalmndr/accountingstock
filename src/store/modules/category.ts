import { GetterTree, MutationTree, ActionTree } from 'vuex';
import { StateInterface } from 'src/store';
import { APIDeleteCategory, APIEditCategory, APIGetAllCategory, APIStoreCategory } from 'src/api/category';
import { successNotify,errorNotify } from 'src/utils/notify';

export interface Category {
  name: string,
  minQuantity: number,
  quantity: number,
  category: string,
  price: number,
  type: string
}

//Store Start
const getters: GetterTree<Category, StateInterface> = {};
const state: any = {
  listOfCategory: [],
  category: {}
};
const mutations = {
  setListOfCategory(state, payload) {
    state.listOfCategory = payload;
  },
  setCategory(state, payload) {
    state.account = payload;
  }
};
const actions = {
  async loadCategory({ commit }) {
    try{
    const { data, message } = await APIGetAllCategory();
    // successNotify(message);
    commit('setListOfCategory', data);
    }
    catch(e){
      errorNotify(e);
    }
  },
  async addCategory({ commit }, data) {
    try{
    const { message } = await APIStoreCategory(data);
    successNotify(message);
    }
    catch(e){
      errorNotify(e);
    }
  },
  async deleteCategory({ commit }, id) {
    try{
    const res = await APIDeleteCategory(id);
    successNotify('Data deleted Successfully');
    }
    catch(e){
      errorNotify(e);
    }
  },
  async editCategory({ commit }, data) {
    try{
    const res = await APIEditCategory(data.id, data);
    successNotify('Data Edited Successfully');
    }
    catch(e){
      errorNotify(e);
    }
  }
};

//exprt all data
export default {
  getters,
  state,
  mutations,
  actions
};
