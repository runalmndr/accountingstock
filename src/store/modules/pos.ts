import { ActionTree, GetterTree, MutationTree } from 'vuex';
import { StateInterface } from 'src/store';
import {  APIGetAllPos } from 'src/api/inventory';
// import { POSITEMS } from 'src/utils/constants';
// import { log } from 'util';
// import { fabStackExchange } from '@quasar/extras/fontawesome-v5';
import { errorNotify, successNotify } from 'src/utils/notify';

export interface IPos {
  items: any;
  selectedItems: any;
}

const iPos: IPos = {
  items: [],
  selectedItems: []
};

//Store Start
const getters: GetterTree<IPos, StateInterface> = {};

const state = {
  ...iPos
};
const mutations: MutationTree<IPos> = {
  pushToSelectedItems(state, { id, value }) {
    const index = state.selectedItems.findIndex(v=> v.id == id);
    const stateVal= state.selectedItems[index];
    stateVal.orderQty = [1,-1].includes(value) ? stateVal.orderQty + value : value;
    !stateVal.orderQty ? stateVal.orderQty=0 : "";
    stateVal.total= stateVal.orderQty * (stateVal.unitPrice? stateVal.unitPrice : stateVal.price);
  },
  setPrice(state,{id,value}){
    const index = state.selectedItems.findIndex(v=> v.id == id);
    const stateVal= state.selectedItems[index];
    stateVal.unitPrice= value;
    stateVal.total= stateVal.unitPrice * stateVal.orderQty ;
  },
  setTotal(state,{id,val}){
    const index = state.selectedItems.findIndex(v=> v.id == id);
    const stateVal= state.selectedItems[index];
    stateVal.total=val;
  },
  setItems(state,payload){
    state.items=payload;
  },
  setSelectedItems(state,payload){
    state.selectedItems=payload;
  }
};

const actions: ActionTree<IPos, StateInterface> = {
  async loadItems({ commit }) {
    try{
    const { data, message } = await APIGetAllPos();
    commit('setItems', data);
    commit('setSelectedItems',data.map(v=>({...v,orderQty:0,unitPrice:0,total:0})));
    }
    catch(e){
      errorNotify(e);
    }
  },
};

export default {
  getters,
  state,
  mutations,
  actions
};
