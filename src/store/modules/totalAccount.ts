import { ITotalAccount } from 'src/utils/models/interfaces/TotalAccount';
import { mockTotalAccount } from 'src/utils/mock/Reports';
import {GetterTree , MutationTree , ActionTree} from "vuex"
import { StateInterface } from 'src/store';
export interface ItotalAccout{
  totalAccounts: ITotalAccount,
  randomVal:string
}
const totalAcc: ITotalAccount = mockTotalAccount;


//Store Start
const getters:GetterTree<ITotalAccount , StateInterface>={}
const state : ItotalAccout = {
totalAccounts : totalAcc,
  randomVal:'321 lets go'
}
const mutations : MutationTree<ItotalAccout>= {}
const actions : ActionTree<ItotalAccout, StateInterface> = {}

//exprt all data
export default {
  getters,
  state,
  mutations,
  actions
}
