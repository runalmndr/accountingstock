import { GetterTree, MutationTree, ActionTree } from 'vuex';
import { StateInterface } from 'src/store';
import {
  APIGetAllJournal,
  APIEditJournal,
  APIStoreJournal,
  APIDeleteJournal

} from 'src/api/journal';
import { errorNotify, successNotify } from 'src/utils/notify';

export interface BooksOfTransaction {
  name: string;
  balance: number;
}

//Store Start
const getters: GetterTree<BooksOfTransaction, StateInterface> = {};
const state: any = {
  listOfJournal: [],
    journal: {}
};
const mutations = {
  setListOfJournal(state, payload) {
    state.listOfJournal = payload;
  },
  setTransaction(state, payload) {
    state.journal = payload;
  }
};
const actions = {
  async loadEntry({ commit }) {
    try{
    const { data, message } = await APIGetAllJournal();
    // successNotify(message);
    commit('setListOfJournal', data);
    }
    catch(e){
      errorNotify(e);
    }
  },

  async addEntry({ commit }, data) {
    try{
      const { message } = await APIStoreJournal(data);
      successNotify(message);
    }
    catch(e){
      errorNotify(e,'Unable to add Entry');
    }
  },
  async deleteEntry({ commit }, id) {
    try {
      const res = await APIDeleteJournal(id);
      successNotify(`Data deleted Successfully ${{ res }}`);
    } catch (e) {
      errorNotify('Unable to delete transaction', e);
    }
  },
  async editEntry({ commit }, data) {
    try{
    const res = await APIEditJournal(data._id, data);
    successNotify('Data deleted Successfully');
    }
    catch(e){
      errorNotify(e);
    }
  }
};

//exprt all data
export default {
  getters,
  state,
  mutations,
  actions
};
