import { RouteConfig } from 'vue-router';

const routes = store => {
  return [
    {
      path: '/',
      component: () => import('layouts/MainLayout.vue'),
      meta: {
        requiresAuth: true
      },
      children: [
        {
          path: '',
          name: 'point of Sales',
          component: () => import('src/pages/POS.vue')
        },
        {
          path: 'journal',
          name: 'journal',
          component: () => import('src/pages/journalEntry.vue')
        },
        {
          path: 'books-of-account',
          name: 'books of accounts',
          component: () => import('src/pages/Accounts.vue')
        },
        {
          path: 'day-book',
          name: 'day book',
          component: () => import('src/pages/Daybook.vue')
        },
        {
        path: 'pos',
          name: 'pos',
          component: () => import('src/pages/Daybook.vue')
        },
        {
          path: 'inventory',
          name: 'inventory',
          component: () => import('src/pages/Inventory.vue')
        },
        {
          path: 'stock',
          name: 'day-book',
          component: () => import('src/pages/Daybook.vue')
        },
        {
          path: 'credit-management',
          name: 'credits',
          component: () => import('src/pages/Credits.vue')
        },
        {
          path: 'salary',
          name: 'salary',
          component: () => import('src/pages/Salary.vue')
        }, {
          path: 'employee',
          name: 'employee',
          component: () => import('src/pages/Employee.vue')
        }, {
          path: 'warehouse',
          name: 'warehouse',
          component: () => import('src/pages/Warehouse.vue')
        },
        {
          path: 'billEdit/:id',
          name: 'billEdit',
          component: () => import('src/pages/BillEdit.vue')
        },
        {
          path: 'bill',
          name: 'bill',
          component: () => import('src/pages/Bills.vue')
        },
        {
          path: 'recipient',
          name: 'recipient',
          component: () => import('src/pages/Recipient.vue')
        },
        {
          path: 'settings',
          name: 'settings',
          component: () => import('src/pages/Daybook.vue')
        }
      ]
    },
    {
      path: '*',
      meta: {
        requiresAuth: false
      },
      component: () => import('pages/Error404.vue')
    }
  ];
};

// Always leave this as last one,
// but you can also remove it

export default routes;
