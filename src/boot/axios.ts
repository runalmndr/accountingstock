import axios, { AxiosInstance } from 'axios';
import { boot } from 'quasar/wrappers';
import { errorNotify, successNotify } from 'src/utils/notify';

declare module 'vue/types/vue' {
  interface Vue {
    $axios: AxiosInstance;
  }
}
export default boot(({ Vue, store, router }) => {
  // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
  // axios.defaults.baseURL = 'http://localhost:8181/api';
  axios.defaults.baseURL = 'https://api.apukhata.com/api';
  // axios.defaults.baseURL = '';
  axios.defaults.headers.common.Authorization = store.getters['user/getToken']
    ? `${store.getters['user/getToken']}`
    : '';

  axios.defaults.headers['Accept-Language'] = store.getters['user/getLocale']
    ? store.getters['user/getLocale']
    : 'en-us';

// Add a request interceptor
  axios.interceptors.request.use(
    function(config) {
      // Do something before request is sent
      return config;
    }, // function(config)
    function(error) {
      // Do something with request error
      return Promise.reject(error);
    } // function(error)
  ); // axios.interceptors.request.use

  axios.interceptors.response.use(
    function(response) {
      // Any status code that lie within the range of 2xx cause this function to trigger
      // Do something with response data
      // response.status === 200 && !!response.data.message && successNotify(response.data.message);
      return response.data;
    }, // function(response)
    function(error) {
      // Any status codes that falls outside the range of 2xx cause this function to trigger
      // Do something with response error
      if (error?.response?.status === 403) {
        errorNotify(error?.response?.data?.message, error);
        return;
      }
      if (error?.response?.status === 401) {

        errorNotify(error?.response?.data?.message, error);
        return;
        // store.dispatch('user/clearSession');
        // router.go(0);
      }
      if (error?.response?.status === 400) {

        errorNotify(error?.response?.data?.message, error);
        return;
        // store.dispatch('user/clearSession');
        // router.go(0);
      }


      return Promise.reject(error?.response?.data?.message);
    } // function(error)
  ); // axios.interceptors.response.use
  Vue.prototype.$axios = axios;
});


