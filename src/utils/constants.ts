export const POSITEMS = [
  {
    // image: 'http://dummyimage.com/207x100.png/cc0000/ffffff',
    id: 1,
    name: 'Stellaria borealis Bigelow ssp. sitchana (Steud.) Piper',
    price: 500,
    inventory:100
  },
  {
    // image: 'http://dummyimage.com/118x100.png/cc0000/ffffff',
    id: 2,
    name: 'Sapium Jacq.',
    price: 150,
    inventory:100
  },
  {
    // image: 'http://dummyimage.com/106x100.png/cc0000/ffffff',
    id: 3,
    name: 'Buxbaumia minakatae Okam.',
    price: 450,
    inventory:100
  },
  {
    // image: 'http://dummyimage.com/197x100.png/ff4444/ffffff',
    id: 4,
    name: 'Sclerocarpus uniserialis (Benth.) Hemsl. var. uniserialis',
    price: 900,
    inventory:100
  },
  {
    // image: 'http://dummyimage.com/182x100.png/dddddd/000000',
    id: 5,
    name: 'Cyanea aspleniifolia (H. Mann) Hillebr.',
    price: 450,
    inventory:100
  },
  {
    // image: 'http://dummyimage.com/135x100.png/5fa2dd/ffffff',
    id: 6,
    name: 'Tetraclinis Masters',
    price: 259,
    inventory:100
  },
  {
    // image: 'http://dummyimage.com/214x100.png/cc0000/ffffff',
    id: 7,
    name: 'Stereocaulon grande (H. Magn.) H. Magn.',
    price: 902,
    inventory:100
  },
  {
    // image: 'http://dummyimage.com/212x100.png/ff4444/ffffff',
    id: 8,
    name: 'Gymnocarpium disjunctum (Rupr.) Ching',
    price: 458,
    inventory:100
  },
  {
    // image: 'http://dummyimage.com/197x100.png/cc0000/ffffff',
    id: 9,
    name: 'Lappula squarrosa (Retz.) Dumort.',
    price: 985,
    inventory:55
  },
  {
    // image: 'http://dummyimage.com/174x100.png/dddddd/000000',
    id: 10,
    name: 'Tetraclinis Masters',
    price: 259,
    inventory:55
  },
  {
    // image: 'http://dummyimage.com/214x100.png/cc0000/ffffff',
    id: 11,
    name: 'Stereocaulon grande (H. Magn.) H. Magn.',
    price: 902,
    inventory:50
  },
  {
    // image: 'http://dummyimage.com/212x100.png/ff4444/ffffff',
    id: 12,
    name: 'Gymnocarpium disjunctum (Rupr.) Ching',
    price: 458,
    inventory:50
  },
  {
    // image: 'http://dummyimage.com/197x100.png/cc0000/ffffff',
    id: 13,
    name: 'Lappula squarrosa (Retz.) Dumort.',
    price: 985,
    inventory:100
  },
  {
    // image: 'http://dummyimage.com/174x100.png/dddddd/000000',
    id: 14,
    name: 'Acacia microcarpa F. Muell.',
    price: 100,
    inventory:100
  }
];
