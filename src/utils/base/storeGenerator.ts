import { Quasar as q }  from 'quasar'
import { store } from 'quasar/wrappers';

interface StoreGenerator {
  actions: any,
  mutations: any,
  state: any
}




export const generateStoreValues = (moduleName: string): StoreGenerator => {
  // const baseAPIS = generateAPIS(moduleName);

  return {
    actions: {
      [`getAll${moduleName}`]:async ()=>{
        // const res= await baseAPI.getAllValues();
         q.store.commit(`set${moduleName}`)``
        }
    },
    mutations: {},
    state: {}
  };
};
