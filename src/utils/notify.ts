import { Notify } from 'quasar';

export const successNotify=(message:string)=>{
  Notify.create({
    type:'positive',
    message:message,
    position:'top-left'
  })
}
export const errorNotify=(message:string = 'Erro in System', error='system error')=>(Notify.create({
    type:'negative',
    message:message,
    position:'top-left'
}));
  
