import dateConverter from 'bs-ad-convertor';
export const nepToEng = (val: any, type = 'eng', arr = val.split('-')) => {
  const { year = 0, month = 0, date = 0 } =
    type === 'eng'
      ? dateConverter.nep_to_eng(+arr[0], +arr[1], +arr[2])
      : dateConverter.eng_to_nep(+arr[0], +arr[1], +arr[2]);
  return [year, month, date].join('-').toString();
};
