import { IS_DEV } from 'src/utils/env';
import { random } from 'src/utils/randomNumberGenerator';
import { BASIS } from 'src/utils/mock/CATEGORIES';
import { IRates } from 'src/utils/models/interfaces/Rates';

export const mockRate: IRates = IS_DEV ? {
  category: 'John Doe',
  amount: 1000,
  basis: 'Electricity',
  rateCategory: 'M1'
} : {
  category: '',
  amount: 0,
  basis: '',
  rateCategory: ''
};

export const mockRates = (num: number): IRates[]=>{
  let i = 1;
  const data:IRates[]= [];
  while(i<=num){
    data.push({
      category: mockRate.category + i.toString(),
      amount:  random(1,9,1000),
      basis: BASIS[random(0,BASIS.length-1)],
      rateCategory: mockRate.rateCategory + i
    });
    i++;
  }
  return data;
}
