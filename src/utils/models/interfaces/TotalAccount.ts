import { IBaseEntity } from 'src/utils/models/base';

export interface ITotalAccount extends IBaseEntity{
  customerId: string;
  fullName?: string;
  amount?: number;
  meterCategory?: string;
  readingDate?: Date,
}

export interface ITotalAccountDTO extends ITotalAccount{
  type?: string;
}
