export interface IRates{
  category: string;
  amount: number;
  basis: string;
  rateCategory: string;
}
