
export interface IBaseEntity {
  id?: number;
  created?: Date;
  updated?: Date;
}

