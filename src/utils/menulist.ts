export const MENULIST = [
  {
    path: '/',
    name: 'Point of Sales'
  },
  {
    path: 'books-of-account',
    name: 'Books of Accounts'
  },
  {
    path: 'day-book',
    name: 'Day Book'
  },
  {
    path: 'inventory',
    name: 'Inventory'
  },{
    path: 'employee',
    name: 'Employee'
  },
  {
    path: 'credit-management',
    name: 'Credits'
  },
  {
    path: 'salary',
    name: 'Salary'
  },
  {
    path: 'journal',
    name: 'Journal'
  }, {
    path: 'bill',
    name: 'Bill'
  }, {
    path: 'recipient',
    name: 'Recipient'
  },
  // {
  //   path: 'billEdit',
  //   name: 'billEdit'
  // },
  // {
  //   path: 'warehouse',
  //   name: 'Warehouse'
  // },
  // {
  //   path: 'settings',
  //   name: 'Settings'
  // }

];
