import { DeleteRequest, GetRequest, PostRequest, PutRequest } from 'src/plugins/http';

export const APIGetAllAccounts = () => {
  return GetRequest('/books-of-account');
};

export const APIGetOpeningClosing = () => {
  return GetRequest('/books-of-account/opening-closing');
};

export const APIGetEmployeeRecipient = () => {
  return GetRequest('/books-of-account/getCreditors');
};


export const APIStoreAllAccount = (data) => {
  return PostRequest('/books-of-account', data);
};

export const APIDeleteAccount = (id) => {
  return DeleteRequest('/books-of-account/' + id,{});
};

export const APIEditAccount = (id, data) => {
  return PutRequest('/books-of-account/' + id, data);
};
