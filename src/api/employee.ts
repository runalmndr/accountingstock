import { DeleteRequest, GetRequest, PostRequest, PutRequest } from 'src/plugins/http';

export const APIGetAllEmployees = () => {
  return GetRequest('/employee');
};

export const APIGetEmployees = () => {
  return GetRequest('/employee/name');
};


export const APIStoreAllEmployee = (data) => {
  return PostRequest('/employee', data);
};

export const APIDeleteEmployee = (id) => {
  return DeleteRequest('/employee/' + id,{});
};

export const APIEditEmployee = (id, data) => {
  return PutRequest('/employee/' + id, data);
};
