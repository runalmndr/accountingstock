import { PostRequest } from 'src/plugins/http';

export const APIAuthenticate = ({ email, password }) => {
  return PostRequest('auth/login', { email, password });
};
export const APIAuthenticatePass = ({ password , oldPassword, userId}) => {
  return PostRequest('auth/changePassword', { userId, password, oldPassword });
};
