import { DeleteRequest, GetRequest, PostRequest, PutRequest } from 'src/plugins/http';

export const APIGetAllCategory = () => {
  return GetRequest('/category');
};


export const APIStoreCategory = (data) => {
  return PostRequest('/category', data);
};

export const APIDeleteCategory = (id) => {
  return DeleteRequest('/category/' + id,{});
};

export const APIEditCategory = (id, data) => {
  return PutRequest('/category/' + id, data);
};
