import { DeleteRequest, GetRequest, PostRequest, PutRequest } from 'src/plugins/http';

export const APIGetAllWarehouse = () => {
  return GetRequest('/warehouse');
};


export const APIStoreWarehouse = (data) => {
  return PostRequest('/warehouse', data);
};

export const APIDeleteWarehouse = (id) => {
  return DeleteRequest('/warehouse/' + id,{});
};

export const APIEditWarehouse = (id, data) => {
  return PutRequest('/warehouse/' + id, data);
};
