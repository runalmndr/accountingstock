import { DeleteRequest, GetRequest, PostRequest, PutRequest } from 'src/plugins/http';

export const APIGetAllJournal = () => {
  return GetRequest('/journal');
};


export const APIStoreJournal = (data) => {
  return PostRequest('/journal/', data);
};


export const APIEditJournal = (id, data) => {
  return PutRequest('/transaction/' + id, data);
};

export const APIDeleteJournal = (id) => {
  return DeleteRequest('/transaction/' + id,{});
};
