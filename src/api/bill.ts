import { DeleteRequest, GetRequest, PostRequest, PutRequest } from 'src/plugins/http';


export const APIGetAllBill = () => {
  return GetRequest('/bill');
};

export const APIStoreBill = (data) => {
  return PostRequest('/bill', data);
};
export const APIStoreBillOrderItems = (id,data) => {
  return PostRequest('/order-item/addNewOrder/'+id, data);
};

export const APIDeleteBill = (id) => {
  return DeleteRequest('/bill/' + id,{});
};
export const APIDeleteBillOrderItem = (billId,orderId) => {
  return DeleteRequest(`/order-item/${billId}/${orderId}/`,{});
};

export const APIEditBill =async (id, data) => {
  return await PutRequest('/bill/' + id, data);
};

export const APIEditBillOrderItem = (id, data) => {
  return PutRequest('/order-item/' + id, data);
};
export const APIGetBillById = (id) => {
  return GetRequest('/bill/' + id);
};

export const APIUpdateAmtPaid = (id, data) => {
  return PutRequest('/bill/updateAmountPaid/' + id, data);
};
