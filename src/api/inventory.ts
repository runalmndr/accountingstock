import { DeleteRequest, GetRequest, PostRequest, PutRequest } from 'src/plugins/http';

export const APIGetAllInventory = () => {
  return GetRequest('/inventory');
};
export const APIGetAllPos = () => {
  return GetRequest('/inventory/pos');
};


export const APIStoreInventory = (data) => {
  return PostRequest('/inventory', data);
};

export const APIDeleteInventory = (id) => {
  return DeleteRequest('/inventory/' + id,{});
};
export const APIDeleteInventoryComp = (iId,cId) => {
  return DeleteRequest('/inventory/' +iId+'/'+cId,{});
};

export const APIEditInventory = (id, data) => {
  return PutRequest('/inventory/' + id, data);
};

export const APIAddExtraInventoryComp = (id, data) => {
  return PutRequest('/inventory/addComposition/' + id, data);
};

export const APIEditInventoryComp = (id, data) => {
  return PutRequest('/stock-composition/' + id, data);
};
