import { DeleteRequest, GetRequest, PostRequest, PutRequest } from 'src/plugins/http';

export const APIGetAllUsers = () => {
  return GetRequest('/books-of-account/getRecipient');
};

export const APIGetAllRecipientName = () => {
  return GetRequest('/books-of-account/getRecipient-name');
};

export const APIEditRecipient = (rowId,userId,data) => {
  return PutRequest('/user-detail/' + rowId+'/'+userId, data);
};

export const APIStoreUser = (data) => {
  return PostRequest('/books-of-account/credit', data);
};

export const APIDeleteRecipient = (id) => {
  return DeleteRequest('/books-of-account/' + id,{});
};
