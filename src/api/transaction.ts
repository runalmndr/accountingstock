import { DeleteRequest, GetRequest, PostRequest, PutRequest } from 'src/plugins/http';

export const APIGetAllTransactions = () => {
  return GetRequest('/transaction');
};
export const APIGetAllSalary = () => {
  return GetRequest('/transaction/search/Salary')
};

export const APIGetAllCredit = () => {
  return GetRequest('/transaction/search/Credit')
};

export const APIStoreSalary = (data) => {
  return PostRequest('/transaction/salary', data);
};
export const APIStoreCredit = (data) => {
  return PostRequest('/transaction/credit', data);
};

export const APIEditSalary = (id,data) => {
  return PutRequest('/transaction/salary/'+id,data);
};
export const APIEditCredit = (id,data) => {
  return PutRequest('/transaction/credit/'+id,data);
};

export const APIStoreAllTransaction = (data) => {
  return PostRequest('/transaction/day-book', data);
};

export const APIStoreAllJournal = (data) => {
  return PostRequest('/journal', data);
};


export const APIDeleteTransaction = (id) => {
  return DeleteRequest('/transaction/' + id,{});
};

export const APIEditTransaction = (id, data) => {
  return PutRequest('/transaction/' + id, data);
};
